package models

data class CrossoverEdge(val value: Int, var doubled: Boolean = false)