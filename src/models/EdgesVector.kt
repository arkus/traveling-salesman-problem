package models

class EdgesVector {
    private var vector: MutableMap<Pair<Int, Int>, Int> = mutableMapOf()

    fun addEdge(from: Int, to: Int, distance: Int) {
        vector[Pair(from, to)] = distance
        vector[Pair(to, from)] = distance
    }

    fun distance(from: Int, to: Int): Int {
        return vector.getOrDefault(Pair(from, to), -1)
    }

    fun size() = vertices().size

    fun vertices() = vector.keys.map { it.first}.distinct()

    override fun toString(): String {
        return vector.toString()
    }

    fun randomOrder(): List<Int> {
        val keys = vertices().toMutableList()
        keys.shuffle()
        return keys
    }
}