package models

data class Arguments(
        val populationSize: Int,
        val inputFile: String,
        val iterationsCount: Int
)