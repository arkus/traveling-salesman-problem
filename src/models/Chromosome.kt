package models

data class Chromosome(val data: List<Int>)