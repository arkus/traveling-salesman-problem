package helpers

import models.Chromosome
import models.EdgesVector

object DistanceCalculator {
    fun calculateTotalDistance(chromosome: Chromosome, edgesVector: EdgesVector): Int {
        var sum = 0
        chromosome.data.forEachIndexed { index, vertex ->
            var to = -1
            to = if (index == chromosome.data.size - 1) {
                chromosome.data.first()
            } else {
                chromosome.data[index + 1]
            }
            sum += edgesVector.distance(vertex, to)
        }
        return sum
    }
 }