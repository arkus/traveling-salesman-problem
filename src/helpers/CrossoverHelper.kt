package helpers

import models.Chromosome
import models.EdgesVector
import java.util.*

object CrossoverHelper {
    fun crossover(parents: Pair<Chromosome, Chromosome>, edgesVector: EdgesVector): Pair<Chromosome, Chromosome> {
        val edgesMap: MutableMap<Int, CrossoverEdges> = mutableMapOf()
        edgesVector.vertices().forEach {
            edgesMap[it] = CrossoverEdges()
        }

        readEdges(parents.first, edgesMap)
        readEdges(parents.second, edgesMap)

        return createChild(edgesMap, edgesVector) to createChild(edgesMap, edgesVector)
    }

    private fun createChild(edgesMap: MutableMap<Int, CrossoverEdges>, edgesVector: EdgesVector): Chromosome {
        val list: MutableList<Int> = mutableListOf()

        val random = Random()
        val randomVertex = edgesVector.vertices()[random.nextInt(edgesVector.size())]
        list.add(randomVertex)

        while (list.size != edgesVector.size()) {
            val edges = edgesMap[list.last()]!!.edges
            var shortestListCount = edgesVector.size()
            val possibleEdges = mutableListOf<Int>()

            for (i in 0 until edges.size) {
                val edge = edges[i]
                if (list.contains(edge.first)) {
                    continue
                }
                if (edge.second) {
//                    println("common edge!")
                    possibleEdges.add(edge.first)
                    break
                }
                val size = edgesMap[edge.first]!!.edges.size
                if (size < shortestListCount) {
                    shortestListCount = size
                    possibleEdges.apply {
                        clear()
                        add(edge.first)
                    }
                } else if (size == shortestListCount) {
                    possibleEdges.add(edge.first)
                }
            }

            when {
                possibleEdges.size == 1 -> //                println("chosen: ${possibleEdges.first()}")
                    list.add(possibleEdges.first())
                possibleEdges.isEmpty() -> {
                    var randomlyChosen = edgesVector.vertices()[random.nextInt(edgesVector.size())]
                    while (list.contains(randomlyChosen)) {
                        randomlyChosen = edgesVector.vertices()[random.nextInt(edgesVector.size())]
                    }
                    list.add(randomlyChosen)
                }
                else -> {
                    val randomlyChosen = possibleEdges[random.nextInt(possibleEdges.size)]
                    //                println("chosen (random from $possibleEdges): $randomlyChosen")
                    list.add(randomlyChosen)
                }
            }
        }

//        println("result: $list")
        return Chromosome(list)
    }

    private fun readEdges(parent: Chromosome, edgesMap: MutableMap<Int, CrossoverEdges>) {
        parent.data.forEachIndexed { index, value ->
            when (index) {
                0 -> {
                    edgesMap[value]?.add(parent.data[parent.data.size - 1])
                    edgesMap[value]?.add(parent.data[index + 1])
                }
                parent.data.size - 1 -> {
                    edgesMap[value]?.add(parent.data[index - 1])
                    edgesMap[value]?.add(parent.data[0])
                }
                else -> {
                    edgesMap[value]?.add(parent.data[index - 1])
                    edgesMap[value]?.add(parent.data[index + 1])
                }
            }

        }
    }

    fun findParents(population: List<Chromosome>): List<Pair<Chromosome, Chromosome>> {
        val shuffled = population
        val halfOfPopulation = shuffled.take(population.size / 2)
        val otherHalf = shuffled.takeLast(population.size / 2)

        val parents: MutableList<Pair<Chromosome, Chromosome>> = mutableListOf()

        for (i in 0 until otherHalf.size) {
            parents.add(halfOfPopulation[i] to otherHalf[i])
        }

        return parents
    }

    class CrossoverEdges {
        val edges: MutableList<Pair<Int, Boolean>> = mutableListOf()

        fun add(value: Int) {
            val pair = value to false
            val indexOf = edges.indexOf(pair)
            if (indexOf != -1) {
                edges.removeAt(indexOf)
                edges.add(value to true)
            } else {
                edges.add(pair)
            }
            edges.sortBy { !it.second }
        }

        override fun toString(): String {
            return edges.toString()
        }
    }
}