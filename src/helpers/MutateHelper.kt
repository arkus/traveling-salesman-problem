package helpers

import models.Chromosome
import java.util.*
import kotlin.math.abs

object MutateHelper {
    fun mutate(chromosome: Chromosome): Chromosome {
        val randomIndexes = findRandomNumbers(chromosome.data.size)
        val sublist = chromosome.data.subList(randomIndexes.first, randomIndexes.second).toMutableList().reversed()
        val newData = chromosome.data.toMutableList()
        for (i in randomIndexes.first until randomIndexes.second) {
            newData[i] = sublist[i - randomIndexes.first]
        }
        return Chromosome(newData)
    }

    private fun findRandomNumbers(size: Int): Pair<Int, Int> {
        val random = Random()
        val first = random.nextInt(size)
        var second = random.nextInt(size)
        while (first == second || abs(first - second) == 1) {
            second = random.nextInt(size)
        }

        return if (first < second) {
            first to second
        } else {
            second to first
        }
    }
}