package helpers

import models.EdgesVector
import java.io.File

object EdgesParser {
    fun parse(inputFile: String): EdgesVector {
        val file = File(inputFile)
        val inputStream = file.inputStream()
        val edgesVector = EdgesVector()

        inputStream.bufferedReader().useLines {
            it.forEach {
                val values = it.split(" ").map { it.toInt() }
                edgesVector.addEdge(values[0], values[1], values[2])
            }
        }
        return edgesVector
    }
}