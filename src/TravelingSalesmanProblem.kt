import helpers.CrossoverHelper
import helpers.DistanceCalculator
import helpers.EdgesParser
import helpers.MutateHelper
import models.Arguments
import models.Chromosome
import models.EdgesVector
import java.util.*

fun main(args: Array<String>) {
    val arguments = parseArguments(args)
    val edges = EdgesParser.parse(arguments.inputFile)
    var population = generateInitialPopulation(arguments.populationSize, edges)

    val initialDistances = population.map { DistanceCalculator.calculateTotalDistance(it, edges) }
    println("Initial distances: $initialDistances")

    var iterationNumber = 0

    var bestChromosome = Chromosome(listOf())
    var indexOfBest = 0
    var distances = listOf<Int>()

    while (iterationNumber < arguments.iterationsCount) {
        println("---------------------------")
        println("---- Iteration no. ${String.format("%03d", iterationNumber)} ----")
        println("---------------------------")
        distances = population.map { DistanceCalculator.calculateTotalDistance(it, edges) }
        val fitnesses = distances.map { fitness(it) }
        indexOfBest = fitnesses.indexOf(fitnesses.max())
        bestChromosome = population[indexOfBest]

        println("Start distances: $distances")
        println("Best chromosome: $bestChromosome")
        println("Best distance: ${distances[indexOfBest]}")

        val parents = selectParents(population, edges)
        val children = crossover(parents, edges)
        val mutated = mutate(children, edges)
        population = mutated

        iterationNumber += 1
    }

    println("---------------------------")
    println("---------- END ------------")
    println("---------------------------")
    println("Best chromosome: $bestChromosome")
    println("Best distance: ${distances[indexOfBest]}")
}

fun mutate(children: MutableList<Chromosome>, edges: EdgesVector): MutableList<Chromosome> {
    val threshold = 0.003
    val random = Random()

    for (i in 0 until children.size) {
        if (random.nextFloat() < threshold) {
            println("mutating ${children[i]}")
            println("distance before: ${DistanceCalculator.calculateTotalDistance(children[i], edges)}")
            children[i] = MutateHelper.mutate(children[i])
            println("after mutation: ${children[i]}")
            println("distance after: ${DistanceCalculator.calculateTotalDistance(children[i], edges)}")
        }
    }

    return children
}

fun crossover(parents: MutableList<Chromosome>, edges: EdgesVector): MutableList<Chromosome> {
    val parentsPairs = CrossoverHelper.findParents(parents)

    val children = mutableListOf<Chromosome>()

    parentsPairs.map { CrossoverHelper.crossover(it, edges) }
            .forEach {
                children.add(it.first)
                children.add(it.second)
            }

    return children
}

fun selectParents(population: MutableList<Chromosome>, edges: EdgesVector): MutableList<Chromosome> {
    val parents = mutableListOf<Chromosome>()
    val distances = population.map { DistanceCalculator.calculateTotalDistance(it, edges) }
    val fitnessSum = distances.map { fitness(it) }.reduce { acc, fl -> acc.plus(fl) }

    val predictions = distances.map {
        fitness(it) * fitnessSum
    }.toMutableList()

    val sorted = predictions.sorted().reversed()

    val count = (0.05 * population.size).toInt()

    parents.addAll(sorted.take(count).map { population[predictions.indexOf(it)] })

    val random = Random()
    for (i in 0 until population.size - count) {
        val nextFloat = random.nextFloat()
        val value = Math.min(nextFloat, predictions.sumUpTo(predictions.size))
        predictions.forEachIndexed { index, fl ->
            if (value > predictions.sumUpTo(index - 1) && value <= predictions.sumUpTo(index)) {
                parents.add(population[index])
            }
        }
    }

    return parents
}

fun List<Float>.sumUpTo(n: Int): Float {
    return this.take(n + 1).sum()
}

fun fitness(distance: Int): Float {
    return 1 / distance.toFloat()
}

fun parseArguments(args: Array<String>): Arguments {
    var populationSize = 0
    var iterationsCount = 0
    var citiesFile = ""
    args.forEachIndexed { index, value ->
        when (index) {
            0 -> populationSize = value.toInt()
            1 -> citiesFile = value
            2 -> iterationsCount = value.toInt()
        }
    }

    if (populationSize == 0) {
        throw IllegalArgumentException("population size must be greater than 0")
    }

    if (populationSize % 2 == 1) {
        throw IllegalArgumentException("population size must be odd number")
    }

    if (iterationsCount == 0) {
        throw IllegalArgumentException("iterations count must be greater than 0")
    }

    if (citiesFile.isEmpty()) {
        throw IllegalArgumentException("input file must be not empty")
    }

    return Arguments(populationSize, citiesFile, iterationsCount)
}

fun generateInitialPopulation(populationSize: Int, edges: EdgesVector): MutableList<Chromosome> {
    val population: MutableList<Chromosome> = mutableListOf()


    while (population.size != populationSize) {
        val element = Chromosome(edges.randomOrder())
        population.add(element)
    }

    return population
}

